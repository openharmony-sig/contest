#ifndef PCA9685_H__
#define PCA9685_H__

#include <stdint.h>

#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

void pca9685_init(void);
void pca9685_reset(void);

void pca9685_write(uint8_t channel, float angle);
//float pca9685_read(uint8_t channel);
void pca9685_unload(uint8_t channel);

#endif /* PCA9685_H__ */