cmake_minimum_required(VERSION 3.14)
project(GenkiPi C)

set(CMAKE_C_STANDARD 99)

include_directories(../../utils/native/lite/include)
include_directories(../../base/iot_hardware/peripheral/interfaces/kits)
include_directories(../../device/itcast/genkipi/interfaces/kits)
include_directories(../../device/itcast/genkipi/hi3861_adapter/kal/cmsis)
include_directories(../../device/itcast/genkipi/sdk_liteos/include)
include_directories(../../device/itcast/genkipi/sdk_liteos/third_party/lwip_sack/include)
include_directories(../../device/itcast/genkipi/iot_tool/include)
include_directories(../../drivers/framework/include/platform)
include_directories(../../drivers/framework/include/utils)
include_directories(../../third_party/cJSON/)

include_directories(app/web_ext)
include_directories(app/plotclock)

# 该 CMakeLists.txt 对实际编译没有任何作用，只是在编写代码时方便编辑器进行代码提示
add_executable(GenkiPiSample
        app/hello_world/main.c
        app/led_gpio/main.c
        app/led_pwm/main.c
        app/env_base/main.c
        app/ohosdog/main.c
        app/plotclock/main.c
        app/plotclock/genki_web_plotclock.c
        app/plotclock/kinematics.c
)
