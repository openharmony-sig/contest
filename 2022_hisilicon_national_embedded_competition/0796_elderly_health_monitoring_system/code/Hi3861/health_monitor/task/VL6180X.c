#include "sys.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <ohos_init.h>
#include <cmsis_os2.h>
#include <iot_i2c.h>
#include <iot_gpio.h>
#include <iot_errno.h>

#define Vl6_INTERVAL_TIME_US 300000
#define Vl6_TASK_STACK_SIZE 4096
#define Vl6_TASK_PRIO 25
#define Vl6_TEST_GPIO 9 // for hispark_pegasus

static void *DistanceTask(const char *arg)
{
    (void)arg;
    printf("\r\nVL6180X测距实验\r\n");
	if(VL6180X_Init() == 0)	printf("\r\nVL6180X初始化成功!\r\n");
	delay_us(2000000);
	/*LOOP*/
    u8 ex_Range = 0;
	while(1)
	{
		printf("循环"); 
		ex_Range = VL6180X_Read_Range();
        printf("\r\n Current Range:%d mm",ex_Range);
		delay_us(100000);//100ms
    }
    return NULL;
}

static void DistanceTaskEntry(void)
{
    osThreadAttr_t attr;
   
    attr.name = "DistanceTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = Vl6_TASK_STACK_SIZE;
    attr.priority = Vl6_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)DistanceTask, NULL, &attr) == NULL) {
        printf("[DistanceTask] Falied to create Vl6Task!\n");
    }
}

SYS_RUN(DistanceTaskEntry);