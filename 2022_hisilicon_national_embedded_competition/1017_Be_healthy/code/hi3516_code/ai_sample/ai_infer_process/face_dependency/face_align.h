#ifndef FACE_ALIGN_H
#define FACE_ALIGN_H

#include "ai_infer_process.h"

#if __cplusplus
extern "C" {
#endif

HI_VOID FACE_ALIGN_IVE_PerspTrans(IVE_IMAGE_S *src, IVE_IMAGE_S *dst, HI_U32 DstWidth, HI_U32 DstHeight,const LandMark *orginpts);

#ifdef __cplusplus
}
#endif
#endif
