#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>   //创建客户端

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connect_button_clicked();

    void on_disconnect_button_clicked();

    void on_forward_btn_clicked();

    void on_left_btn_clicked();

    void on_back_btn_clicked();

    void on_right_btn_clicked();

    void on_stop_button_clicked();

    void on_send_parameters_btn_clicked();

    void on_send_parameters_btn_2_clicked();

    void on_start_button_clicked();


    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket* m_tcp;   //用于客户端与服务端通信

};
#endif // MAINWINDOW_H
