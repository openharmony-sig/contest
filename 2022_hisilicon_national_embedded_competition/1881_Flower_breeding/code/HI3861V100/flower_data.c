#include "flower_data.h"
#include <stdint.h>

uint8_t temp_func(uint8_t type,float temp)      //温度处理
{
    if(temp < flower_temp[type][0])             //温度过低
        return 'l';
    else if(temp > flower_temp[type][1])        //温度过高
        return 'h';
    else
        return 'r';                             //温度适中
}

uint8_t ph_func(uint8_t type,float ph)
{
    if(ph < flower_ph[type][0])                 //ph过低
        return 'l';
    else if(ph > flower_ph[type][1])            //ph过高
        return 'h';
    else
        return 'r';                             //ph适中
}

uint8_t hum_func(uint8_t type,float hum)
{
    if(hum < flower_hum[type][0])               //湿度过低
        return 'l';
    else if(hum > flower_hum[type][1])          //湿度过高
        return 'h';
    else
        return 'r';                             //湿度适中
}

float temp_mim(uint8_t type)
{
    float a;
    a = flower_temp[type][0];
    return a;
}

float temp_max(uint8_t type)
{
    float a;
    a = flower_temp[type][1];
    return a;
}

float ph_mim(uint8_t type)
{
    float a;
    a = flower_ph[type][0];
    return a;
}

float ph_max(uint8_t type)
{
    float a;
    a = flower_ph[type][1];
    return a;
}

float hum_mim(uint8_t type)
{
    float a;
    a = flower_hum[type][0];
    return a;
}

float hum_max(uint8_t type)
{
    float a;
    a = flower_hum[type][1];
    return a;
}