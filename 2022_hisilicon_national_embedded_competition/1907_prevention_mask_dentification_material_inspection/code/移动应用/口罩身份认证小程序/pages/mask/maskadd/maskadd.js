Page({
  data: {
    cTime: "",
    face:"",
    data:"",
  },
  onLoad: function (e) {
    var that = this
    var newDate = new Date().toLocaleString("zh-CN", {
      hour12: false
    }).replaceAll('/', '-');
    newDate = newDate.substr(0, newDate.lastIndexOf(':'));
    console.log(newDate);
    that.setData({
      cTime: newDate
    })
  },
  facepic:function() {
    var that=this
    wx.request({
      url: 'https://dlut.cpolar.cn/photo',
      success:function (res) {
        console.log(res.data)
        that.setData({
          face:res.data.ans
        })
      }
    })
  },
  //提交
  submit: function (e) {
    var that = this
    if(that.data.face=="") {
      wx.showToast({
        title: '未获取图像',
        icon: 'none'
      })
      return;
    }
    wx.request({
      url: 'https://dlut.cpolar.cn/mask',
      data: {
        url:that.data.face,
        time:that.data.cTime
      },
      success:function (res) {
        console.log(res.data)
        that.setData({
          data:res.data.ans
        })
      }
    })
  }
})