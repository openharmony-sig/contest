
#ifndef APP_DEMO_I2C_OLED_H
#define APP_DEMO_I2C_OLED_H

unsigned int OledInit(void);
void OledSetPosition(unsigned char x, unsigned char y);
void OledFillScreen(unsigned char fiiData);
void OledPositionCleanScreen(unsigned char fillData, unsigned char line,
    unsigned char pos, unsigned char len);
void OledShowChar(unsigned char x, unsigned char y, unsigned char chr, unsigned char charSize);
void OledShowStr(unsigned char x, unsigned char y, unsigned char *chr, unsigned char charSize);
unsigned char *FlaotToString(float d, unsigned char *str);
void AllLedOff(void);
#endif