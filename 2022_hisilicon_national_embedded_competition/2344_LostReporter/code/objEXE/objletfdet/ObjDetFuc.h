﻿


#pragma once

#ifdef _MSC_VER && __cplusplus
#define EXPORTFLAG  extern "C" __declspec(dllexport)
#else
#define EXPORTFLAG extern "C"
#endif

//°æ±¾ºÅ
#define VERSTRING "ObjDet.1.0.20190917"

#define _MYDEBUG_ 0


#include <errno.h>
#include <fcntl.h>
#include "string.h"
#include "stdlib.h"
#include "math.h"

#include "stdio.h"


#define _SUCC 0
#define _FAIL -1


typedef struct _strObjDetPara
{
	int width;//图像宽度
	int height;//图像高度
	int GMM_LEARN_FRAME; //背景建模缓存帧数，默认500

} strObjDetPara;

void YUV420sptoRGB(unsigned char* yuvBuf, myImage * output, unsigned int nWidth, unsigned int nHeight);
/*
初始化函数
输入：Para：参数
输出：无
返回值：句柄，句柄为空时表示初始化失败
*/

EXPORTFLAG void* ObjDetInit(strObjDetPara *Para);

/*
检测函数
输入：
    hdl：句柄
	yuv420：yuv420格式视频图像帧数据
	width：图像宽度
	height：图像高度
输出：无
返回值：0：无警情，>0：有警情
*/
EXPORTFLAG int  ObjDet(void *hdl, VIDEO_FRAME_INFO_S *video_frame, int width, int height);
/*
释放函数
输入：
	hdl：句柄
输出：无
返回值：0：成功，-1：失败
*/
EXPORTFLAG int ObjDetClose(void *hdl);









