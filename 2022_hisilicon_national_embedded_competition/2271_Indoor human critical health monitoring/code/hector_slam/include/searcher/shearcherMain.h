#ifndef _SHEARCHERMAIN_H
#define _SHEARCHERMAIN_H
#include "AstarSearch.h"
#include "pathOptimiz.h"

Eigen::Vector2d getPosPoly( Eigen::MatrixXd polyCoeff, int k, double t );

void* pathShearchThread(void*);
void startSearch(Eigen::Vector2d start_pt, Eigen::Vector2d target_pt);
Eigen::MatrixXd getOptimizPath();


#endif
