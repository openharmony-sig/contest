#ifndef _LIDAR_MAIN_H
#define _LIDAR_MAIN_H

#include "generaltype.h"

bool lidarDataIsReady(void);
HiRobot::LaserScan lidarGetData(void);
void* readLidarThread(void*);
void* getSerialPort();
#endif