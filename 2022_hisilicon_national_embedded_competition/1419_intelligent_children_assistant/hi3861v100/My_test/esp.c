#include "esp.h"

void Esp8266_GpioInit()
{
    unsigned int ret;
    unsigned char Rdata[100] = {0};
    unsigned char *Wdata = "flag==1";
    IoTGpioInit(U1_RX);
    IoTGpioInit(U1_TX);
    IoSetFunc(U1_RX, 2);
    IoSetFunc(U1_TX, 2);
    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8,      /* dataBits: 8bits */
        .stopBits = 1,      /* stop bit */
        .parity = 0,
    };
    ret = IoTUartInit(HI_UART_IDX_1, &uartAttr);

    printf("%s", AT1);
    IoTUartWrite(HI_UART_IDX_1, AT1, strlen(AT1) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", AT2);
    IoTUartWrite(HI_UART_IDX_1, AT2, strlen(AT2) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", AT3);
    IoTUartWrite(HI_UART_IDX_1, AT3, strlen(AT3) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", AT4);
    IoTUartWrite(HI_UART_IDX_1, AT4, strlen(AT4) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);
    printf("%s", AT5);
    IoTUartWrite(HI_UART_IDX_1, AT5, strlen(AT5) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", AT6);
    IoTUartWrite(HI_UART_IDX_1, AT6, strlen(AT6) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", AT7);
    IoTUartWrite(HI_UART_IDX_1, AT7, strlen(AT7) + 1);
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);

    printf("%s", Wdata);
    IoTUartWrite(HI_UART_IDX_1, Wdata, strlen(Wdata));
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
    osDelay(500);
    printf("%s", AT8);
    IoTUartWrite(HI_UART_IDX_1, AT8, strlen(AT8));
    IoTUartRead(HI_UART_IDX_1, Rdata, 100);
}

extern uint8_t RobotMode;

static void *EspTask(void *parame)
{
    unsigned char Rdata[50] = {0};
    unsigned short i = 0;
    (void)parame;
    printf("Esp task start\r\n");
    while (1)
    {
        if (IoTUartRead(HI_UART_IDX_1, Rdata, 50))
        {
            if (strstr((const char *)Rdata, "PROGRAM_MODE"))
            {
                RobotMode = PROGRAM_MODE;
            }
            else if (strstr((const char *)Rdata, "MONITOR_MODE"))
            {
                RobotMode = MONITOR_MODE;
            }
            if (RobotMode == PROGRAM_MODE || RobotMode == MONITOR_MODE)
            {
                if (str_analyse(Rdata))
                {
                    printf("done\r\n");
                }
            }
            memset(Rdata, 0, 50);
        }
        else
        {
            if (i == 250)
            {
                IoTUartWrite(HI_UART_IDX_1, "this is robot\r\n", strlen("this is robot\r\n"));
                i = 0;
            }
        }
        i++;
        osDelay(1);
    }
}

void EspEntry(void)
{
    osThreadAttr_t attr;
    Esp8266_GpioInit();
    attr.name = "EspTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = ESP_TASK_STACK_SIZE;
    attr.priority = 24;

    if (osThreadNew((osThreadFunc_t)EspTask, NULL, &attr) == NULL)
    {
        printf("[EspEntry] Falied to create EspTask!\n");
    }
}

SYS_RUN(EspEntry);