#ifndef __HAND_H__
#define __HAND_H__

#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "control.h"

#define P1_GPIO 2
#define P2_GPIO 6
#define P3_GPIO 7
#define P4_GPIO 9
#define P6_GPIO 12

#define HAND_TASK_STACK_SIZE 1024 * 5

#define COUNT 2

void Hand_GpioInit();
void P1_set_angle(float angle);
void P2_set_angle(float angle);
void P3_set_angle(float angle);
void P4_set_angle(float angle);
void P6_set_angle(float angle);

#endif