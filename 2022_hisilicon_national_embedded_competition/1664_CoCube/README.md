
作品设计报告为本团队对于CoCube作品的设计介绍

code为本作品的udp和pwm控制核心代码

将code文件夹中udp_demo 文件夹放在./OpenHarmony_master/applications/sample/wifi-iot/app

并在./OpenHarmony_master/applications/sample/wifi-iot/app/BUILD.gn中feature索引下增加

     features = [
        "udp_demo:udpServer",
    ]
即可编译此代码。

其中motion2.h为PWM初始化与PWM运动控制函数

udp_server.c为udp控制主函数

其余代码为udp相关配置函数

进行ip配置与端口配置请详细阅读相关代码