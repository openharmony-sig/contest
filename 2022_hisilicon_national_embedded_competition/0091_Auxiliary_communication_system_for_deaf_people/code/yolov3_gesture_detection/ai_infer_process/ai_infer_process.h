/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AI_INFER_PROCESS_H
#define AI_INFER_PROCESS_H

#include <stdint.h>
#include "sample_comm_nnie.h"

#if __cplusplus
extern "C" {
#endif

#define HI_PER_BASE         100
#define HI_OVEN_BASE        2 // Even base

/* rect box */
typedef struct RectBox {
    int xmin;
    int xmax;
    int ymin;
    int ymax;
} RectBox;

/* recognized number information */
typedef struct RecogNumInfo {
    uint32_t num; // Recognized numeric value, 0~9
    uint32_t score; // The credibility score of a number, the value range of which is defined by a specific model
} RecogNumInfo;

/* plug related information */
typedef struct AiPlugLib {
    int width;
    int height;
    uintptr_t model;
} AiPlugLib;

/* Information of detected objects */
typedef struct DetectObjInfo {
    int cls; // The category of the object, an integer> 0
    RectBox box; // The rectangular area of the object (pixels)
    float score; // Object's credibility score
} DetectObjInfo;


//这些宏是从 sample_svp_nnie_software.h里复制过来的，避免依赖那个头文件
#define SAMPLE_SVP_NNIE_ADDR_ALIGN_16 16 /* 16 byte alignment */
#define SAMPLE_SVP_NNIE_MAX(a, b) (((a) > (b)) ? (a) : (b))
#define SAMPLE_SVP_NNIE_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define SAMPLE_SVP_NNIE_SIGMOID(x) (HI_FLOAT)((1.0f) / (1 + exp(-(x))))

#define SAMPLE_SVP_NNIE_COORDI_NUM 4                         /* coordinate numbers */
#define SAMPLE_SVP_NNIE_PROPOSAL_WIDTH 6                     /* the number of proposal values */
#define SAMPLE_SVP_NNIE_QUANT_BASE 4096                      /* the base value */
#define SAMPLE_SVP_NNIE_SCORE_NUM 2                          /* the num of RPN scores */
#define SAMPLE_SVP_NNIE_X_MIN_OFFSET 0                       /* the offset of RPN min x */
#define SAMPLE_SVP_NNIE_Y_MIN_OFFSET 1                       /* the offset of RPN min y */
#define SAMPLE_SVP_NNIE_X_MAX_OFFSET 2                       /* the offset of RPN max x */
#define SAMPLE_SVP_NNIE_Y_MAX_OFFSET 3                       /* the offset of RPN max y */
#define SAMPLE_SVP_NNIE_SCORE_OFFSET 4                       /* the offset of RPN scores */
#define SAMPLE_SVP_NNIE_SUPPRESS_FLAG_OFFSET 5               /* the offset of suppress flag */
#define SAMPLE_SVP_NNIE_BBOX_AND_CONFIDENCE 5                /* bbox coordinate and confidence */
#define SAMPLE_SVP_NNIE_HALF 0.5f                            /* the half value */
#define SAMPLE_SVP_NNIE_YOLOV3_REPORT_BLOB_NUM 3             /* yolov3 report blob num */
#define SAMPLE_SVP_NNIE_YOLOV3_EACH_GRID_BIAS_NUM 6          /* yolov3 bias num of each grid */
#define SAMPLE_SVP_NNIE_YOLOV3_EACH_BBOX_INFER_RESULT_NUM 29//85 /* yolov3 inference result num of each bbox */

/* stack for sort */
typedef struct hiSAMPLE_SVP_NNIE_STACK {
    HI_S32 s32Min;
    HI_S32 s32Max;
} SAMPLE_SVP_NNIE_STACK_S;

typedef struct hiSAMPLE_SVP_NNIE_YOLOV2_BBOX {
    HI_FLOAT f32Xmin;
    HI_FLOAT f32Xmax;
    HI_FLOAT f32Ymin;
    HI_FLOAT f32Ymax;
    HI_S32 s32ClsScore;
    HI_U32 u32ClassIdx;
    HI_U32 u32Mask;
} SAMPLE_SVP_NNIE_YOLOV2_BBOX_S;

typedef SAMPLE_SVP_NNIE_YOLOV2_BBOX_S SAMPLE_SVP_NNIE_YOLOV3_BBOX_S;


/* creat yolo3 model basad mode file */
int Yolo3Create(SAMPLE_SVP_NNIE_CFG_S **model, const char* modelFile);

/* destory yolo3 model */
void Yolo3Destory(SAMPLE_SVP_NNIE_CFG_S *self);

/* cal U8C3 image */
int Yolo3CalImg(SAMPLE_SVP_NNIE_CFG_S* self,
    const IVE_IMAGE_S *img, DetectObjInfo resBuf[], int resSize, int* resLen);



#ifdef __cplusplus
}
#endif
#endif
