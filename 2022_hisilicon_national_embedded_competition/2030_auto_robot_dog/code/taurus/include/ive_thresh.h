/*
 * @Author: Shaohan Weng 
 * @Email: shaohanweng@outlook.com
 */

#ifndef IVE_THRESH_H
#define IVE_THRESH_H
#include "hi_type.h"

/* function : show Kcf sample */
HI_VOID IVE_Thresh(HI_VOID);

/* function : kcf sample signal handle */
HI_VOID IVE_Thresh_HandleSig(HI_VOID);

#endif
