///////////////////////该文件为加速度计在线标定文件,函数用于实现在线标定功能////////////////////////////////////
#include "stdafx.h"
#include "stdio.h"
#include "math.h"


/////////////////////////////////全局变量定义//////////////////////////
double acce_basis[3];//将加表的零偏定义为全局变量方便导航解算

///////////////////////////////////////////////////////////////////////////



int  matrix_inv( double a_matrix[3][3], int ndimen )
///////////////////////////////////////////////////////////////////////////////
// matrix_m:  进行矩阵的求逆运算
///////////////////////////////////////////////////////////////////////////////
{
	double tmp, tmp2, b_tmp[20], c_tmp[20];
	int k, k1, k2, k3, j, i, j2, i2, kme[20], kmf[20];
	
	for( k=0;  k<ndimen; k++ )  {
        tmp2=0.0;
        for( i=k; i<ndimen; i++ )  {
			for( j=k; j<ndimen; j++ )  {
				if( fabs(a_matrix[i][j] ) <= fabs(tmp2) ) continue;
				tmp2=a_matrix[i][j];
				i2=i;
				j2=j;
        }  }
        if( fabs(tmp2) < 1.0e-18 )  return(2);
		
        if( i2 != k ) {
			for( j=0; j<ndimen; j++ )   {
                tmp=a_matrix[i2][j];
                a_matrix[i2][j]=a_matrix[k][j];
                a_matrix[k][j]=tmp;
			}
        }
        if( j2 != k ) {
			for( i=0; i<ndimen; i++ )  {
				tmp=a_matrix[i][j2];
				a_matrix[i][j2]=a_matrix[i][k];
				a_matrix[i][k]=tmp;
        }    }
		
        kme[k]=i2;
        kmf[k]=j2;
        for( j=0; j<ndimen; j++ )  {
			if( j==k )   {
				b_tmp[j]=1.0/tmp2;
				c_tmp[j]=1.0;
			}
			else {
				b_tmp[j]=-a_matrix[k][j]/tmp2;
				c_tmp[j]=a_matrix[j][k];
			}
			a_matrix[k][j]=0.0;
			a_matrix[j][k]=0.0;
        }
		
        for( i=0; i<ndimen; i++ )  {
			for( j=0; j<ndimen; j++ )  {
				a_matrix[i][j]=a_matrix[i][j]+c_tmp[i]*b_tmp[j];
        }  }
	}
	
	for( k3=0; k3<ndimen;  k3++ )   {
        k=ndimen-k3-1;
        k1=kme[k];
        k2=kmf[k];
        if( k1 != k )   {
            for( i=0; i<ndimen; i++ )  {
				tmp=a_matrix[i][k1];
				a_matrix[i][k1]=a_matrix[i][k];
				a_matrix[i][k]=tmp;
		}  }
		if( k2 != k )   {
			for( j=0; j<ndimen; j++ )  {
				tmp=a_matrix[k2][j];
				a_matrix[k2][j]=a_matrix[k][j];
				a_matrix[k][j]=tmp;
			}
		}
	}
	return( 0 );
}




void acce_cali(double amatrix[3][3])
{
    int ag;//无特殊意义，仅作为求逆函数的一个输出量存放
	int i,j;//计数量
    double z[3];//存放m-1的值，m为均方差的值
	double bmatrix[3][3];//用于存放转置矩阵

	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		bmatrix[i][j]=amatrix[i][j];
	}
	
	ag=matrix_inv(bmatrix,3);//将矩求逆

    z[0]=amatrix[0][0]*amatrix[0][0]+amatrix[0][1]*amatrix[0][1]+amatrix[0][2]*amatrix[0][2]-1;
	z[1]=amatrix[1][0]*amatrix[1][0]+amatrix[1][1]*amatrix[1][1]+amatrix[1][2]*amatrix[1][2]-1;
	z[2]=amatrix[2][0]*amatrix[2][0]+amatrix[2][1]*amatrix[2][1]+amatrix[2][2]*amatrix[2][2]-1;//计算z值

//////////////////////////////////进行加表偏置的计算///////////////////////////

	acce_basis[0]=0.5*(bmatrix[0][0]*z[0]+bmatrix[0][1]*z[1]+bmatrix[0][2]*z[2]);
	acce_basis[1]=0.5*(bmatrix[1][0]*z[0]+bmatrix[1][1]*z[1]+bmatrix[1][2]*z[2]);
	acce_basis[2]=0.5*(bmatrix[2][0]*z[0]+bmatrix[2][1]*z[1]+bmatrix[2][2]*z[2]);

	//FILE *testtt;
	//testtt=fopen("testtt.dat","w");
	//fprintf(testtt,"%f %f %f %f %f %f %f %f %f/n",amatrix[0][0],amatrix[0][1],amatrix[0][2],amatrix[1][0],amatrix[1][1],amatrix[1][2],amatrix[2][0],amatrix[2][1],amatrix[2][2]);
	//fprintf(testtt,"%f/t%f/t%f/t%f/t%f/t%f/t%f/t%f/t%f/n",bmatrix[0][0],bmatrix[0][1],bmatrix[0][2],bmatrix[1][0],bmatrix[1][1],bmatrix[1][2],bmatrix[2][0],bmatrix[2][1],bmatrix[2][2]);

}



