#include "stdafx.h"
#include "hmr_yaw.h"
#include "math.h"


extern double deg_rad;
extern double pi;


void hmr_yaw(double atti[3],double hmr[3],double hmryaw[1])
{
//////////////////////////////////////////////////////
//////函数名：HMR_YAW
//////参数：atti hmr（参数传入）hmryaw（参数传出）
////实现功能: 根据姿态角和磁传感器的输出，求出磁航向角
////////////////////////////////////////////////////


double ex,ey,exey;


atti[0]=atti[0]*pi/180.0f;
atti[1]=atti[1]*pi/180.0f;
atti[2]=atti[2]*pi/180.0f;


ex=cos(atti[1])*hmr[0]+sin(atti[0])*sin(atti[1])*hmr[1]+cos(atti[0])*sin(atti[1])*hmr[2];
ey=cos(atti[0])*hmr[1]-sin(atti[0])*hmr[2];
exey=fabs(ey/ex);

if (ex>0)
 {
		if (ey>0) 
		{
		hmryaw[0]=360-atan(exey)*180/pi;
		}
		else
		{
		hmryaw[0]=atan(exey)*180/pi;
		}

}
else
{

		if (ey<0) 
		{
		hmryaw[0]=180-atan(exey)*180/pi;
		}
		else
		{
		hmryaw[0]=180+atan(exey)*180/pi;
		}


}

atti[0]=atti[0]*180.0/pi;
atti[1]=atti[1]*180.0/pi;
atti[2]=atti[2]*180.0/pi;//姿态角使用完之后应该恢复原来的单位


}
