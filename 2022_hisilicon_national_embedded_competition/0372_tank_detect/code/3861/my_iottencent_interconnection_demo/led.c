
#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"

#define LED_INTERVAL_TIME_US 300
#define LED_TEST_GPIO 9 // for hispark_pegasus


void led_off(void)   //自定义led灯灭
{
    IoTGpioInit(LED_TEST_GPIO);
    IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
    //IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
    //usleep(LED_INTERVAL_TIME_US);
    IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
    usleep(LED_INTERVAL_TIME_US);
    printf("led off \n");
    
}




