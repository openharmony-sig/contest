# 基于视觉的机械手智能交互系统

## 链接地址

<https://gitee.com/yizhixiangfeidemao/qianrushi.git>

本作品利用视觉交互系统，打造智能交互机械手，如图所示。

![image](D:\LearnGraduate\2022-hisilicon-national-embedded-competition\media\4.png)

智能手势识别：

![image](D:\LearnGraduate\2022-hisilicon-national-embedded-competition\media\12.png)

智能手势交互：

![image](D:\LearnGraduate\2022-hisilicon-national-embedded-competition\media\14.png)

智能抓取物体：

![image](D:\LearnGraduate\2022-hisilicon-national-embedded-competition\media\3.png)