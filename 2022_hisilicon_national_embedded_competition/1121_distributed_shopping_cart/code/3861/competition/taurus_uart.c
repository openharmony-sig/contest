#include <stdio.h> 
#include <unistd.h> 
#include "ohos_init.h" 
#include "cmsis_os2.h" 
#include "iot_gpio.h" 
#include "iot_gpio_ex.h" 
#include "iot_uart.h" 
#include <hi_gpio.h>
#include <hi_io.h>
#include <hi_uart.h>
#include "crc32.h"
#include "hx711.h"
#include "hi_time.h"
#include "items_msg.h"
#include "iot_log.h"


extern int weight_state;       //0：无变化 1：检测中 2：删除成功 3：添加成功 
extern hi_u16 customer_code;
unsigned char uartWriteBuff[256] = {0};
unsigned char uartReadBuff[256] = {0};
Crc32Type Crc32_Msg={0};

#define DEMO_UART_NUM1  HI_UART_IDX_1

static void Uart1GpioCOnfig(void)
{
    IoSetFunc(IOT_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(IOT_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
}


int usr_uart_config(void) 
{ 
    int ret; 
 
    //初始化UART配置，115200，数据bit为8,停止位1，奇偶校验为NONE，流控为NONE 
    IotUartAttribute uartAttr1 = {
        .baudRate = 115200, 
        .dataBits = 8, 
        .stopBits = 1, 
        .parity = 0,
    };
    ret = IoTUartInit(DEMO_UART_NUM1, &uartAttr1);
    if (ret != 0) {
        printf("uart1 init fail = %d\r\n", ret); 
    } 
    return ret; 
} 
 
void interconnect_msg_deal(Crc32Type *crc32_msg)
{
    int i;
    hi_u16 vip_code;printf("\n4");
    vip_code = (crc32_msg->MsgBuf[0])*256+(crc32_msg->MsgBuf[1]);printf("\n5");
    if(vip_code >= 32768)
        customer_code = vip_code;
} 

void Taurus_Write(hi_u16 data)
{
    hi_u32 crc32_code = 0;
    int len = 2;
    unsigned char buffer[32]={0};
    buffer[0] = 0xaa;
    buffer[1] = 0x55;
    buffer[2] = 0x00;
    buffer[3] = 0x02;
    buffer[4] = data/256;
    buffer[5] = data%256;
    buffer[6] = 0xff;printf("\n1");
    crc32_code = Crc32_Get(buffer, (len + 5));
    buffer[len + 5] = (hi_u8) ((crc32_code & 0xff000000)>>24);
    buffer[len + 6] = (hi_u8) ((crc32_code & 0x00ff0000)>>16);
    buffer[len + 7] = (hi_u8) ((crc32_code & 0x0000ff00)>>8);
    buffer[len + 8] = (hi_u8)   crc32_code;
    IoTUartWrite(DEMO_UART_NUM1, buffer, len+9);
}

//1.任务处理函数 
static void* Uart1_Task(const char* arg) 
{ 
    unsigned int i;
    unsigned int len = 0; 
    unsigned int len_send = 0;
    unsigned int weight_state_new=0;
    unsigned int weight_state_old=0;
    Uart1GpioCOnfig(); 
    usr_uart_config();
    (void)arg; 
    while (1) { 
        weight_state_new = weight_state;
        if(weight_state_old == 1)
            if(weight_state_new == 2)
            {
                Taurus_Write(2);  //sub success
            }
            else if(weight_state_new == 3)
            {
                Taurus_Write(3); //add success
            }
        weight_state_old = weight_state_new;

        len = IoTUartRead(DEMO_UART_NUM1, uartReadBuff, sizeof(uartReadBuff));
        if(len > 4)
        { 
            //printf("\ntaurus:");
            //for(i=0;i<len;i++)    printf(" %X",uartReadBuff[i]);
       
            memset(&Crc32_Msg , 0 , sizeof(Crc32_Msg));
            if(UartMsg_Receive( &Crc32_Msg , uartReadBuff , len))
            {printf("\n3");
                interconnect_msg_deal(&Crc32_Msg);
                len_send = Uart_DataPackage(&Crc32_Msg , uartWriteBuff , Crc32_Msg.MsgLen[0]*256+Crc32_Msg.MsgLen[1]);
                IoTUartWrite(DEMO_UART_NUM1, uartWriteBuff, len_send); 
            }
        }
        hi_sleep(2000); 
    } 
  
    return NULL; 
} 
 
//2.任务入口函数 
static void Uart1_Demo(void) 
{ 
    osThreadAttr_t attr = {0}; 
 
    printf("[UartDemo] UartDemo1_Entry()\n"); 
 
    attr.name = "TaurusDemo"; 
    attr.attr_bits = 0U; 
    attr.cb_mem = NULL; 
    attr.cb_size = 0U; 
    attr.stack_mem = NULL; 
    attr.stack_size = 4028;//堆栈大小 
    attr.priority = 24;//优先级 
 
    if (osThreadNew((osThreadFunc_t)Uart1_Task, NULL, &attr) == NULL) 
    { 
        printf("[UartDemo] Falied to create UartDemo_Task1!\n"); 
    } 
} 
 
//3.注册模块 
SYS_RUN(Uart1_Demo); 
