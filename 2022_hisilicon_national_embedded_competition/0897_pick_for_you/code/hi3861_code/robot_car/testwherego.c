#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/*
 * Copyright (C) 2022 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http:// www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"
#include "hi_adc.h"
#include "iot_errno.h"
#include "robot_hcsr04.h"
#include "robot_l9110s.h"
#include "robot_sg90.h"
#include "trace_model.h"
#include "robot_control.h"

#define     GPIO5 5
#define     FUNC_GPIO 0
#define     IOT_IO_PULL_UP 1
#define     VLT_MIN                     (100)
#define     OLED_FALG_ON                ((unsigned char)0x01)
#define     OLED_FALG_OFF               ((unsigned char)0x00)
unsigned short  g_gpio5_adc_buf[ADC_TEST_LENGTH] = {0 };
unsigned int    g_gpio5_tick = 0;
unsigned int    g_car_control_demo_task_id = 0;
unsigned char   g_car_status = CAR_STOP_STATUS;

void testwherego(void)
{
    int i;
    unsigned int time = 10000;//5000对应5秒
    while(1)
    {
    // hi_sleep(time);
    car_forward();
    hi_sleep(time);
    car_backward();
    hi_sleep(time);
    car_left();
    hi_sleep(time);
    car_right();
    hi_sleep(time);
    car_stop();
    }
}
APP_FEATURE_INIT(testwherego);