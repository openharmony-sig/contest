#ifndef PAGE2_H
#define PAGE2_H

#include <QWidget>
#include <QDebug>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>
extern QFont font1,font2,font3,font4;
namespace Ui {
class Page2;
}

class Page2 : public QWidget
{
    Q_OBJECT

public:
    explicit Page2(QWidget *parent = 0);
    ~Page2();
    void setListWidget(QString message);
private:
    Ui::Page2 *ui;
};

#endif // PAGE2_H
