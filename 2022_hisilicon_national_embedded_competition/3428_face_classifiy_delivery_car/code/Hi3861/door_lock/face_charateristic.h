#ifndef FACE_RECOGNIZE_H
#define FACE_RECOGNIZE_H
typedef enum
{
    Hzh_Face =        0x00020001,
    Wjm_Face =        0x00020002,
    BackGround_B =    0x00020003,
    Unkown_Face =     0x00020004
}FaceGather_t;
typedef union 
{
    FaceGather_t Recognize;
    char         byte[4];
}FaceCharacteristic_t;
#endif