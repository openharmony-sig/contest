#include <iostream>

#include <opencv2/opencv.hpp>

using namespace cv;

HI_S32 VideoStreamCapture()
{
    Mat frame;
    Mat dstimg;

    // 视频路径
    VideoCapture capture("");

    // cout<<"start opening:"<<endl;

    if(capture.isOpened())
    {
        capture >> frame;
        if (frame.empty())
            break;  //判断视频是否读取结束
        // 修改图片的尺寸
        resize(frame,dstimg,cv::Size(1280,720),cv::INTER_LINEAR);
        frame.release();
        // imshow("show", dstimg);
        // 将图片保存在选择的路径中
        cv::imwrite("/userdata/car.jpg", dstimg);
        // cout<<"Take the "<<i<<" picture"<<endl;
    }
    
    // cout<<"end opening:"<<endl;
    return 0;
}
