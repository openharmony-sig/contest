import inspect
import math
import os
import sys

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0,currentdir)

"""
Implementation of Resnet+LSTM
"""
class Res_LSTM(nn.Module):
    def __init__(self, sample_size=256, sample_duration=16, num_classes=100,
                lstm_hidden_size=512, lstm_num_layers=1, arch="resnet18"):
        super(Res_LSTM, self).__init__()
        self.sample_size = sample_size
        self.sample_duration = sample_duration
        self.num_classes = num_classes

        # network params
        self.lstm_hidden_size = lstm_hidden_size
        self.lstm_num_layers = lstm_num_layers

        # network architecture
        if arch == "resnet18":
            resnet = models.resnet18(pretrained=True)
        elif arch == "resnet34":
            resnet = models.resnet34(pretrained=True)
        elif arch == "resnet50":
            resnet = models.resnet50(pretrained=True)
        elif arch == "resnet101":
            resnet = models.resnet101(pretrained=True)

        # delete the last fc layer 去掉ResNet最后一个全连接层
        modules = list(resnet.children())[:-1] # .children(): 返回网络模型里的组成元素(返回的是最外层的元素)
        self.resnet = nn.Sequential(*modules)
        self.lstm = nn.LSTM(
            input_size=resnet.fc.in_features, # LSTM的输入维度 = ResNet全连接层的输入维度
            hidden_size=self.lstm_hidden_size,
            num_layers=self.lstm_num_layers,  # LSTM输出维度 = 分类类别数量
            batch_first=True,)

        self.fc1 = nn.Linear(self.lstm_hidden_size, self.num_classes)

    def forward(self, x):
        # CNN
        cnn_embed_seq = []
        # x: (batch_size, channel, t, h, w)
        for t in range(x.size(2)):
            # with torch.no_grad():
            out = self.resnet(x[:, :, t, :, :])
            # print(out.shape)
            out = out.view(out.size(0), -1)
            cnn_embed_seq.append(out)

        cnn_embed_seq = torch.stack(cnn_embed_seq, dim=0)
        # print(cnn_embed_seq.shape)
        # batch first
        cnn_embed_seq = cnn_embed_seq.transpose_(0, 1)

        # LSTM
        # use faster code paths
        self.lstm.flatten_parameters()
        out, (h_n, c_n) = self.lstm(cnn_embed_seq, None)
        # MLP
        out = self.fc1(out[:, -1, :])

        return out


# Test
if __name__ == '__main__':
    import sys
    sys.path.append("..")
    import torchvision.transforms as transforms
    from dataset import CSL_Isolated
    
    sample_size = 128
    sample_duration = 16
    num_classes = 500
    transform = transforms.Compose([transforms.Resize([sample_size, sample_size]), transforms.ToTensor()])
    dataset = CSL_Isolated(data_path="D:\Works\Hisi_codes\SLRDataset_isolate\color_img",
        label_path="D:\Works\Hisi_codes\SLRDataset_isolate\gloss_label_2.txt", frames=sample_duration,
        num_classes=num_classes, transform=transform)
        
    Res_lstm_net = Res_LSTM(sample_size=sample_size, sample_duration=sample_duration, num_classes=num_classes)
    print(Res_lstm_net(dataset[0]['data'].unsqueeze(0)))
    print()

