var wxCharts = require("../../utils/wxcharts.js");//相对路径
const app = getApp()
var data1 = [10,20,30,40];

Page({
    data: {
        productId: app.globalData.productId,
        deviceName: app.globalData.deviceName,
        stateReported: {},
        action1:0,
        action2:0,
        action3:0,
        action4:0,
        imageWidth:0,
      }, 
    onLoad:function(options){
      // 页面初始化 options为页面跳转所带来的参数
      var _this = this;
      _this.update();
    },

    
    update() {
        wx.showLoading()
        wx.cloud.callFunction({
          name: 'iothub-shadow-query',
          data: {
            ProductId: app.globalData.productId,
            DeviceName: app.globalData.deviceName,
            SecretId: app.globalData.secretId,
            SecretKey: app.globalData.secretKey,
          },
          success: res => {
            wx.showToast({
              icon: 'none',
              title: '获取数据成功！',
            })
            let deviceData = JSON.parse(res.result.Data)
            data1[0] = deviceData.payload.state.reported.action1;
            data1[1] = deviceData.payload.state.reported.action2;
            data1[2] = deviceData.payload.state.reported.action3;
            data1[3] = deviceData.payload.state.reported.action4;
            
            this.setData({
              stateReported: deviceData.payload.state.reported,
            })
            wx.showToast({
                icon: 'none',
                title: '获取数据成功！'});

            console.log("result:", deviceData)
          },
          fail: err => {
            wx.showToast({
              icon: 'none',
              title: 'Subscribe失败，获取云端数据失败',
            })
            console.error('[云函数] [iotexplorer] 调用失败：', err)
          }
        })
      },
    onReady:function(){
      // 页面渲染完成
    },
    onShow:function(){

        new wxCharts({
          canvasId: 'columnCanvas',
          type: 'column',
          categories: ['委屈', '害怕', '开心', '紧张'],
          series: [{
              name: '动作统计',
              data: data1
          }],
          yAxis: {
              format: function (val) {
                  return val + '次';
              },
              /*max:400,
              min:0*/
          },
          width: 320,
          height: 200
      });
    },
      

    line(){
        var windowWidth = 320;
      try {
       var res = wx.getSystemInfoSync();
       windowWidth = res.windowWidth;
      } catch (e) {
       console.error('getSystemInfoSync failed!');
      }
      yuelineChart = new wxCharts({ //当月用电折线图配置
       canvasId: 'yueEle',
       type: 'line',
       categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'], //categories X轴
       animation: true,
       series: [{
        name: 'A',
        data: [1, 6, 9, 1, 0, 2, 9, 2, 8, 6, 0, 9, 8, 0, 3, 7, 3, 9, 3, 8, 9, 5, 4, 1, 5, 8, 2, 4, 9, 8, 7],
        format: function (val, name) {
         return val + '';
        }
       }, {
        name: 'B',
        data: [0, 6, 2, 2, 7, 6, 2, 5, 8, 1, 8, 4, 0, 9, 7, 2, 5, 2, 8, 2, 5, 2, 9, 4, 4, 9, 8, 5, 5, 5, 6],
        format: function (val, name) {
         return val + '';
        }
       },
       ],
       xAxis: {
        disableGrid: true
       },
       yAxis: {
        title: '数值',
        format: function (val) {
         return val;
        },
        /*max: 20,*/
        min: 0
       },
       width: windowWidth,
       height: 200,
       dataLabel: false,
       dataPointShape: true,
       extra: {
        lineStyle: 'curve'
       }
      });
      },

    onHide:function(){
      // 页面隐藏
    },
    onUnload:function(){
      // 页面关闭
    }
  })