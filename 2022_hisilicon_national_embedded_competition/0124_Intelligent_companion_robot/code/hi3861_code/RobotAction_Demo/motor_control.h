#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_watchdog.h"


void Low_frePwmInit(unsigned int port,unsigned short duty, unsigned int freq);
void Low_frePwmStart(unsigned int port, unsigned short duty, unsigned int freq);

