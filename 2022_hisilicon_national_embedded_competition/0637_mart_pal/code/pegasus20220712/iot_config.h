/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_CONFIG_H
#define IOT_CONFIG_H

// <CONFIG THE LOG
/* if you need the iot log for the development , please enable it, else please comment it */
#define CONFIG_LINKLOG_ENABLE   1

// < CONFIG THE WIFI
/* Please modify the ssid and pwd for the own */
#define CONFIG_AP_SSID  "nova9F" // WIFI SSID
#define CONFIG_AP_PWD   "18750289513" // WIFI PWD
/* Tencent iot Cloud user ID , password */
#define CONFIG_USER_ID    "JGATHVRHKVHi3861;12010126;fab0a;1657598400000"
#define CONFIG_USER_PWD   "3471ade7fd4ad986b7d979ef6956ac3effa28cb9183ea55fa85dae274aff5c9a;hmacsha256"
#define CN_CLIENTID     "JGATHVRHKVHi3861" // Tencent cloud ClientID format: Product ID + device name
#endif