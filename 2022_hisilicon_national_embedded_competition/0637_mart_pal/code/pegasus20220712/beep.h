#ifndef BEEP_H
#define BEEP_H

#include <stdio.h>
#include <stdlib.h>

/* 蜂鸣器初始化函数 */
hi_void UserBeepInit(void);

/* 蜂鸣器调用函数 */
hi_void UserBeep(int duration);


#endif