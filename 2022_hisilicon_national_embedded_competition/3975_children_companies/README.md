# OpenHarmonyProject

#### 介绍


####文件架构说明
1.  MRobotAPP目录 ：MRobot移动端APP（harmonyOS2.0系统手机）
     基于ArkUI框架编写，类WEB开发，包括控制UI实现全JS编写；
            
2.  MRobotDesign 
     ---model： 模型文件，stl格式为零件，step格式为装配体 
     ---electric： 电气原理图
3.  MRobotWifi_iot   MRobot控制源码；
    基于OpenHarmony1.0.1编写
    工程包含了oled、mpu6050、蓝牙串口、sut03语言识别模块驱动样例；
4. MRobotUnionDevice ：互联设备控制源码
    互联设备有：风扇、门锁、台灯、植物浇水机
    每台设备可受MRobot控制，使用普通蓝牙连接设备，每次只能连接一台，MRobot已设置为主机模式，其他设备开机会自动连接；
    每台设备还提供了mqtt接口，订阅云端主题，（对应配网页面--云端选项），可通过云端实现秒控
 
5.AI_camera
    手势识别编译导出包

