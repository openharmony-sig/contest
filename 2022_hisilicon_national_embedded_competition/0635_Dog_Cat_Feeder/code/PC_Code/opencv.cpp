#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"

using namespace std;
using namespace cv;

int main()
{
/*打开摄像头****************************************************************************************************/
	VideoCapture cap(0); 							//实例化一个对象
	if(cap.isOpened() == 0) 
	{
		cout << "open camera filed" << endl;
		return 0;
	}
	cout << "open camera success" << endl;

	Mat ColorImage; 							//实例化一个对象
	Mat GrayImage;
	vector <Rect> CatFace;							//方框容器
	vector <Rect> DogFace;							//方框容器

/*给级联分类器加载模型****************************************************************************************************/
	CascadeClassifier Cat_Classifier("/usr/share/opencv4/haarcascades/haarcascade_frontalcatface.xml"); 	//实例化一个对象加载猫脸检测模型!!!!!!!!!
	CascadeClassifier Dog_Classifier("/usr/share/opencv4/haarcascades/dog_face.xml"); 			//实例化一个对象加载狗脸检测模型!!!!!!!!!
	
/*开始检测****************************************************************************************************/
	while(1)
	{
		cap >> ColorImage; 
		cvtColor(ColorImage, GrayImage, COLOR_BGR2GRAY);	 	//灰度处理
		equalizeHist(GrayImage, GrayImage); 				//均衡化
		Cat_Classifier.detectMultiScale(GrayImage, CatFace); 		//检测脸部，将其存储于Face容器中
		Dog_Classifier.detectMultiScale(GrayImage, DogFace); 		//检测脸部，将其存储于Face容器中
		if( CatFace.size() )
		{
			rectangle(ColorImage, CatFace[0], Scalar(0, 0, 255)); 	//在GragImage中框出脸
			printf("there is %d cat\n",(int)CatFace.size());
		}
		else if( DogFace.size() )
		{
			rectangle(ColorImage, DogFace[0], Scalar(0, 255, 0)); 	//在GragImage中框出脸
			printf("there is %d dog\n",(int)DogFace.size());
		}
		imshow("cat and dog", ColorImage); 				//显示(在core库中)
		waitKey(10);
	}

	return 0;
}
