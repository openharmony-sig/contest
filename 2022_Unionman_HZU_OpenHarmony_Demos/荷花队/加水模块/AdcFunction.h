#ifndef __ADCFUNCTION_H
#define __ADCFUNCTION_H
#include<stdio.h>
#include<math.h>
double Adc_read(int adc_id)
{
    FILE *fp=NULL;
    int i=0;
    double out=0;
    char data[6];
    fp=fopen("/sys/bus/iio/devices/iio:device0/in_voltage2_raw","r");
    fscanf(fp,"%s",data); //读取电压值
    i=strlen(data);
    for(int j=0;j<i;j++)
    {
        out+=(double)(data[i-1-j]-48)*pow(10.0,j);    //将电压值从字符串形式转换为数字
    }
    out/=1000;
    out=4.095-out;
    return out;
}
#endif
