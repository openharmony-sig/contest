# 基于Open Harmony的智能水质检测器

### 一、背景：

随着人们对水资源和环境卫生可持续管理的愈加看重，人们意识到水资源的珍贵，为了每个人都能便捷轻松地获取一个地方的水质情况，更好地一起去保护水和环境，我们所想开创的项目--便携水质检测器便是由这样的想法而产生。便携水质检测器可以能更好便携方便地去测量水质的情况，便携地携带在身体，将仪器放在水里，通过NFC碰一碰配网去连接到手机，启动仪器，轻松地从手机显示中获得水质信息或者直接从检测器显示屏获取水质信息，更好地能让每个人能随时地了解水质情况，并能记录每一次数据。对水和环境卫生的保护和可持续管理有一定的促进作用。

### 二、符合的联合国17项可持续发展目标：

6.为所有人提供水和环境卫生并对其进行可持续管理

### 三、设备：

PH传感器、浊度传感器、电导率传感器、九联Unionpi Tiger开发板

### 四、搭建开发环境：

开发板环境搭建(Ubuntu)：

- [【九联Unionpi Tiger开发套件】开箱及编译环境搭建-开源基础软件社区-51CTO.COM](https://ost.51cto.com/posts/13294)

- [knowledge_demo_temp: 该仓库用于临时存储知识体系各个场景的样例，当该场景样例达到一定数目之后即单独设置仓库承载。希望共建同学将自己的样例索引放置在合适的场景条目下 - Gitee.com](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/UnionpiTiger_helloworld)

> 搭建的目的：下载unionpi的源代码以及编译环境的搭建和测试，方便接下来的项目编译

### 五、demo测试

#### 1.源码下载以及编译，镜像烧录

1. 下载源码包，并将其解压到源代码的根目录

   - 如图所示![](README.assets/image-20220812142533246.png)

2. 编译源代码并生成镜像

   参考：[docs/UnionpiTiger_helloworld/源码编译.md · OpenHarmony-SIG/knowledge_demo_temp - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/UnionpiTiger_helloworld/源码编译.md)

3. 镜像烧录

   参考：[docs/UnionpiTiger_helloworld/镜像烧录.md · OpenHarmony-SIG/knowledge_demo_temp - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/UnionpiTiger_helloworld/镜像烧录.md)

#### 2.安装hap包至开发板

​	1.打开DevEco Studio 配置自动签名

![](README.assets/image-20220812224445068.png)

​	2.连接开发板到电脑并点击运行

![](README.assets/image-20220812225213847.png)

![](README.assets/image-20220812225333898-16603938887129.png)

#### 3.运行

![](README.assets/afafaefa.jpg)

### 六、遇到的问题与总结

本项目使用了ADC原始数据读取和NAPI的相关技术，将处理后的ADC数据封装给NAPI输出到APP，由APP调取显示并记录

​	开发时遇到的困难：在选定好设备模块后，查询如何与模块进行通讯和调用，小队从0基础开始收集资料开始学习openharmony源码结构，到最后模块成功完善的过程十分艰辛，从搭建环境各种报错再到一步步探索通讯协议，学习如何对模块进行操作，在到最后初步完善设备驱动，中间过程遇到很多难题需要我们攻克。

​	本次成长计划虽然我们的项目含金量不高，但是小组实实在在的通过这次活动学习到在openharmony上的许多知识。



### 七、开发过程安排与瓶颈

​		（开发过程安排与接口说明在"开发流程和接口文档"文件夹中）

#### 开发过程图

![](README.assets/开发流程1-16603979451891.png)

![](README.assets/e7ec3734685f7d1ef8c15b6403ce5ad.png)

### 八、演示视频

​	网址：https://www.bilibili.com/video/BV1AG4y1a7gc/

### 九、项目源码

​	链接：[solution_student_challenge/基于OpenHarmony便携式水质检测器-郑超 · OpenHarmony-SIG/online_event - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony-sig/online_event/tree/master/solution_student_challenge/基于OpenHarmony便携式水质检测器-郑超)
