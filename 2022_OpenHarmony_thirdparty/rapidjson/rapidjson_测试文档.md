## 库功能简介

- 将JSON字符串转化为DOM树，从而利用Document对象各种api对内部Value进行查询和修改，并最终重新转化为JSON字符串输出
  
- 创建Document对象，构造需要的Value，最终转化为JSON字符串输出
  

## 已测试功能

- 将JSON字符串转化为DOM树，从而利用Document对象各种api对内部Value进行查询和修改，并最终重新转化为JSON字符串输出
  
- 创建Document对象，构造需要的Value，最终转化为JSON字符串输出
  

## 测试逻辑

### 通过示例程序测试

example目录下有多个利用RapidJSON库写的示例程序，均已编译并在rk3568开发板上运行成功，运行结果均符合示例程序要求。该文档选择其中的tutotial程序简述测试逻辑作为示例

1. 利用document对象的parse方法将一个json文本字符串解析为document对象。
  
2. 利用document对象的[]运算符和GetString()，GetBool()等方法获取到document中特定的值
  
3. 利用document对象的[]运算符或者SetString()，SetBool()等方法直接修改对应的值
  
4. 利用document对象的AddMember()方法添加新的成员
  
5. 构造PrettyWriter对象将修改后的document对象输出为格式化后的JSON字符串
  

以上为tutorial示例程序逻辑简述，为示例程序添加BUILD.gn编译文件，修改ohos.build配置文件后编译示例程序并利用hdc工具推送到开发板上运行，运行输出结果均符合预期

### 通过单元测试测试

RapidJSON库自带较为完善的单元测试。单元测试依赖googletest第三方库，目前ohos已经适配了googletest库，所以可以直接依赖该库完成单元测试。

通过为单元测试代码编写BUILD.gn编译文件，修改ohos.build配置信息，编译并推送到开发板上运行单元测试，全部338项单元测试均正常通过