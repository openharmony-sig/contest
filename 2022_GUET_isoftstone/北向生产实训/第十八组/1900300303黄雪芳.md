通过本次实训，获得以下常用小技巧。
# 1、引用组件
hml中添加element，并给组件命名x。就能再主div中引用改组件x
# 2、固定div位置
利用position属性。
例如底部导航栏可以
    position: fixed;
    bottom: 0px;
# 3、跳转页面
利用router.push。在js中方法写
  router.push({
            uri:"pages/x/x"
        })
# 4、数组：多个数据书写方式
在js的数据区data中利用x:[],每组数据用花括号括起，一组多个信息用逗号隔开，一对信息用：隔开

例如：actionData: [{
                         "imgSrc": "/common/icon/share.png", "title": "转发"
                     },
                     {
                         "imgSrc": "/common/icon/comment.png", "title": "746"
                     },
                     {
                         "imgSrc": "/common/icon/like.png", "title": "8779"
                     }
        ],

# 5、查看官方文档
（1）用js开发：文档-》api参考-》js api参考-》手机、平板、智慧屏和智能穿戴开发-》js扩展类Web开发范式进行组件的开发-》组件-》

（2）接口：用js开发：文档-》api参考-》js api参考-》》手机、平板、智慧屏和智能穿戴开发-》接口参考

（3）文档-》指南-》开发-》基于js开发-》ui开发

# 6、请求方式
（1）get
（2）put