package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.pojo.Business;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.pojo.User;
import com.itrui.buyitbackend.service.UserService;
import com.itrui.buyitbackend.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Result userLogin(@RequestBody User user){

        try {

            User myUser = userService.userLogin(user.getUserAccount());
            log.info("登录的用户账号："+myUser.getUserAccount());
            if(!user.getUserPassword().equals(myUser.getUserPassword())){
                log.info("登录的用户账号："+user.getUserPassword());
                log.info("登录的用户账号："+myUser.getUserPassword());
                return new Result(Code.GET_ERR,"密码错误");

            }else {
                return new Result(Code.GET_OK,myUser,"登录成功");
            }
        }catch (Exception e){
            if (e.toString().equals("java.lang.NullPointerException")){
                return new Result(Code.GET_ERR,"账号不存在");
            }
        }

        return null;
    };

    @PostMapping("/register")
    public Result userRegister(@RequestBody User user){
        boolean flag = false;
        log.info("用户注册的信息："+user);
        try{

            if(user.getHeadPhoto() == null || user.getHeadPhoto() == ""){
                user.setHeadPhoto("/images/2022.07.05/3.jpg");
            }

            if(user.getUserName() == null || user.getUserName() == ""){
                user.setUserName("买为用户13629");
            }

            user.setUserCreatetime(new Date());
            flag = userService.userRegister(user);
        }catch (Exception e){
            if (e.toString().equals("org.springframework.dao.DuplicateKeyException:")){
                return new Result(Code.SAVE_ERR,"账号重复");
            }

        }

        return new Result(flag?Code.SAVE_OK:Code.SAVE_ERR,flag?"注册成功":"注册失败");
    };


    @PutMapping("/up")
    public Result updataUserPasswprd(@RequestBody User user){

        log.info("修改的密码："+user.getUserPassword());
        log.info("修改的密码的账户："+user.getUserAccount());
        boolean flag = userService.updataUserPasswprd(user);

        return new Result(flag?Code.UPDATE_OK:Code.UPDATE_ERR,flag?"修改成功":"修改失败");
    };

    /**
     * 获取全部用户
     * @return
     */
    @GetMapping()
    public Result getAllUSer(){

        List<User> allUSer = userService.getAllUSer();

        return new Result(Code.GET_OK,allUSer,"查询成功");
    };


    /**
     * 通过账号查找用户
     * @param account
     * @return
     */
    @GetMapping("/account/{account}")
    public Result getUerByAccount(HttpServletRequest request, @PathVariable Integer account){
        String url = request.getRequestURL().toString();
        int indexOf = Util.getIndexOf(url, "/", 3);

        User uerByAccount = userService.getUerByAccount(account);
        log.info("用户信息"+uerByAccount);
        String _pic = uerByAccount.getHeadPhoto();
        String pic = url.substring(0,indexOf) + _pic;
        uerByAccount.setHeadPhoto(pic);

        return new Result(Code.GET_OK,uerByAccount,"查询成功");
    };

    /**
     * 通过id查找用户
     * @param id
     * @return
     */
    @GetMapping("/id/{id}")
    public Result getUserById(HttpServletRequest request, @PathVariable Integer id){
        String url = request.getRequestURL().toString();
        int indexOf = Util.getIndexOf(url, "/", 3);

        User uerByAccount = userService.getUserById(id);
        log.info("用户信息"+uerByAccount);
        String _pic = uerByAccount.getHeadPhoto();
        String pic = url.substring(0,indexOf) + _pic;
        uerByAccount.setHeadPhoto(pic);

        return new Result(Code.GET_OK,uerByAccount,"查询成功");
    };

}
