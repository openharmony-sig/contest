package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.AllMessage;
import com.itrui.buyitbackend.pojo.BlackList;
import com.itrui.buyitbackend.pojo.MessageReceive;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MessageReceiveService {
    /**
     * 保存消息
     * @param message
     * @return
     */
    public boolean saveMessage(MessageReceive message);

    /**
     * 获取保存的信息
     * @param userId
     * @return
     */
    public List<MessageReceive> getMessage(Integer userId);

    /**
     * 删除用户发送的信息
     * @param userId
     * @return
     */
    public Integer delMessgeById(Integer userId,Integer target);

    /**
     * 判断用户是否存在
     * @param userId
     * @return
     */
    public List<MessageReceive> isUserExist(Integer userId);

    /**
     * 添加到黑名单
     * @param userId
     * @param target
     * @return
     */
    public boolean addBlackList(@Param("userId") Integer userId, @Param("target") Integer target,@Param("type" ) Integer type);

    /**
     * 检查对象是否存在黑名单内
     * @param userId
     * @param target
     * @param type
     * @return
     */
    public BlackList isExistenceBlackList(@Param("userId") Integer userId, @Param("target") Integer target, @Param("type" ) Integer type);

    /**
     * 保存到历史消息数据库表
     * @param allMessage
     * @return
     */
    public boolean saveAllMessage(AllMessage allMessage);

    /**
     * 获取全部历史消息
     * @param userId
     * @param target
     * @return
     */
    public List<AllMessage> getAllMessage(@Param("userId") Integer userId,@Param("target") Integer target);

    /**
     * 获取用户未读消息数量
     * @param userId
     * @param target
     * @return
     */
    public Integer getUnreadCount(@Param("userId") Integer userId,@Param("target") Integer target);

    /**
     * 清除历史消息
     * @param userId
     * @param target
     * @return
     */
    public boolean delALlMessage(@Param("userId") Integer userId,@Param("target") Integer target);

    /**
     * 检查用户是否存在黑名单
     * @param userId
     * @param target
     * @param type
     * @return
     */
    public List<BlackList> isBlack(@Param("userId") Integer userId, @Param("target") Integer target, @Param("type" ) Integer type);

    /**
     * 解除黑名单
     * @param userId
     * @param target
     * @return
     */
    public boolean delBlack(@Param("userId") Integer userId,@Param("target") Integer target, Integer type);
}
