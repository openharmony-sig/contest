package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {



    /**
     * 用户登录
     * @param account
     * @return
     */
    public User userLogin(@Param("account") Integer account);

    /**
     * 用户注册
     * @param user
     * @return
     */
    public int userRegister(User user);

    /**
     * 修改密码（忘记密码）
     * @param user
     * @return
     */
    public int updataUserPasswprd(User user);

    /**
     * 获取全部用户
     * @return
     */
    public List<User> getAllUSer();

    /**
     * 通过账号查找用户
     * @param account
     * @return
     */
    public User getUerByAccount(@Param("account") Integer account);

    /**
     * 通过id查找用户
     * @param id
     * @return
     */
    public User getUserById(@Param("id") Integer id);

}
