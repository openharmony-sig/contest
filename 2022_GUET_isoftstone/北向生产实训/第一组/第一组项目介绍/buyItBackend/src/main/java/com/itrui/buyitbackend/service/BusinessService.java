package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.mapper.BusinessMapper;
import com.itrui.buyitbackend.pojo.Business;
import com.itrui.buyitbackend.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface BusinessService {

    /**
     * 通过账号登录
     * @param account
     * @return
     */
    public Business login(Integer account);

    /**
     * 商户注册
     * @param business
     * @return
     */
    public boolean businessRegister(Business business);

    /**
     * 商户信息
     * @param id
     * @return
     */
    public Business getBusinessInfo(Integer id);
}
