import fileio from '@ohos.fileio';
import request from '@ohos.request';
import {HuaweiIdAuthParamsHelper, HuaweiIdAuthManager} from '@hmscore/hms-jsb-account';

import featureAbility from '@ohos.ability.featureAbility';

export default {
    data: {
        title: "",
        TAG:"YZJ: ",
        name:"none",
        rname:"none",
        fname:"none",
        mail:"none",
    },
    onInit() {
    },
    download(add){
        let downloadTask;
        request.download({ url: add ,filepath:"/data/data/com.yzj.account/files/head.jpg"}).then((data) => {
            downloadTask = data;
            console.info(this.TAG+"正在下载"+JSON.stringify(data))
        }).catch((err) => {
            console.error('Failed to request the download. Cause: ' + JSON.stringify(err));
        })
    },
    signin(){
       var signInOption = new HuaweiIdAuthParamsHelper().setId().setProfile().setAuthorizationCode().build();
        HuaweiIdAuthManager.getAuthApi().getSignInIntent(signInOption).then((result)=>{
            // 登录成功，获取用户的华为帐号信息
            console.info(this.TAG+"signIn success");
            console.info(this.TAG+JSON.stringify(result));
            console.info(this.TAG+"账号名称: " + result.getDisplayName());
            console.info(this.TAG+"头像: " + result.getAvatarUri());
            console.info(this.TAG+"姓"+result.getFamilyName());
            console.info(this.TAG+"名字"+result.getGivenName());
            console.info(this.TAG+"邮箱:"+result.getEmail());

            this.name=result.getDisplayName();
            this.fname=result.getFamilyName();
            this.rname=result.getGivenName()
            this.mail=JSON.stringify(result.getEmail());
            this.download(result.getAvatarUri());
            }).catch((error)=>{
            // 登录失败
            console.error(this.TAG+"signIn fail");
            console.error(this.TAG+JSON.stringify(error));
           });
    },
    logout(){
        HuaweiIdAuthManager.getAuthApi().signOut().then((result)=>{
            //帐号退出成功
            console.info(this.TAG+"signOut success");
            console.info(this.TAG+JSON.stringify(result));
        }).catch((error) => {
            //帐号退出失败
            console.error(this.TAG+"signout fail");
            console.error(this.TAG+JSON.stringify(error));
        });

    },
    cancelauthorization()
    {
        HuaweiIdAuthManager.getAuthApi().cancelAuthorization().then((result)=>{
            // 帐号取消授权成功
            console.info(this.TAG+"取消授权成功");
            console.info(this.TAG+JSON.stringify(result));
        }).catch((error) => {
            // 帐号取消授权失败
            console.error(this.TAG+"取消授权失败");
            console.error(this.TAG+JSON.stringify(error));
        });
    },


}
