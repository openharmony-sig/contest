### 1. 权限
```
"reqPermissions": [
      {
        "name": "ohos.permission.GET_WIFI_INFO"
      },
      {
        "name": "ohos.permission.GET_NETWORK_INFO"
      },
      {
        "name": "ohos.permission.INTERNET"
      }
```
### 2. 服务端
```
try (DatagramSocket socket = new DatagramSocket(PORT)){
    DatagramPacket packet = new DatagramPacket(new byte[255], 255);
    while (true) {
        socket.receive(packet);
        String message = new String(packet.getData(), packet.getOffset(), packet.getLength(), CHARSET_NAME);
        System.out.println(message);
    }
} catch (IOException e) {
    e.printStackTrace();
}
```

### 3. 客户端
```
String ip = tfInput.getText();
            NetManager netManager = NetManager.getInstance(getContext());

            if (!netManager.hasDefaultNet()) {
                return;
            }
            NetHandle netHandle = netManager.getDefaultNet();

            // 通过Socket绑定来进行数据传输
            DatagramSocket socket = null;
            try {
                InetAddress address = netHandle.getByName(ip);
                socket = new DatagramSocket();
                netHandle.bindSocket(socket);
                byte[] buffer = "hello你好".getBytes(CHARSET_NAME);
                DatagramPacket request = new DatagramPacket(buffer, buffer.length, address, PORT);
                // 发送数据
                socket.send(request);
            } catch(IOException e) {
                HiLog.error(LABEL_LOG, "exception happened.");
            }finally {
                if (socket != null) {
                    socket.close();
                }
            }
```